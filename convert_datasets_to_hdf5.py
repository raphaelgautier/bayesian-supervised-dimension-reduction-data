from os import read
from h5py import File
from numpy.core.fromnumeric import compress
from pandas import read_csv

datasets = {
    # "CRM_450kgrid_twist_50DV_subsonic": {
    #     "num_inputs": 50,
    #     "num_outputs": 10,
    #     "has_gradients": False,
    # },
    # "CRM_450kgrid_twist_50DV_transonic": {
    #     "num_inputs": 50,
    #     "num_outputs": 10,
    #     "has_gradients": False,
    # },
    # "elliptic_pde": {"num_inputs": 100, "num_outputs": 2, "has_gradients": True},
    # "hiv": {"num_inputs": 27, "num_outputs": 21, "has_gradients": True},
    # "naca0012": {"num_inputs": 18, "num_outputs": 2, "has_gradients": True},
    # "onera_m6": {"num_inputs": 50, "num_outputs": 2, "has_gradients": True},
    # "quadratic_function_10_inputs_1_active_dims": {
    #     "num_inputs": 10,
    #     "num_outputs": 1,
    #     "has_gradients": True,
    # },
    # "quadratic_function_10_inputs_2_active_dims": {
    #     "num_inputs": 10,
    #     "num_outputs": 1,
    #     "has_gradients": True,
    # },
    # "quadratic_function_25_inputs_1_active_dims": {
    #     "num_inputs": 25,
    #     "num_outputs": 1,
    #     "has_gradients": True,
    # },
    # "quadratic_function_25_inputs_2_active_dims": {
    #     "num_inputs": 25,
    #     "num_outputs": 1,
    #     "has_gradients": True,
    # },
    # "quadratic_function_50_inputs_1_active_dims": {
    #     "num_inputs": 50,
    #     "num_outputs": 1,
    #     "has_gradients": True,
    # },
    # "quadratic_function_50_inputs_2_active_dims": {
    #     "num_inputs": 50,
    #     "num_outputs": 1,
    #     "has_gradients": True,
    # },
    # "quadratic_function_100_inputs_1_active_dims": {
    #     "num_inputs": 100,
    #     "num_outputs": 1,
    #     "has_gradients": True,
    # },
    # "quadratic_function_100_inputs_2_active_dims": {
    #     "num_inputs": 100,
    #     "num_outputs": 1,
    #     "has_gradients": True,
    # },
    # "RAE2822_baseline_51DV_M0.725": {
    #     "num_inputs": 51,
    #     "num_outputs": 6,
    #     "has_gradients": False,
    # },
    "quadratic_function_10_inputs_5_active_dims": {
        "num_inputs": 10,
        "num_outputs": 1,
        "has_gradients": True,
    },
    "quadratic_function_25_inputs_5_active_dims": {
        "num_inputs": 25,
        "num_outputs": 1,
        "has_gradients": True,
    },
    "quadratic_function_50_inputs_5_active_dims": {
        "num_inputs": 50,
        "num_outputs": 1,
        "has_gradients": True,
    },
    "quadratic_function_100_inputs_5_active_dims": {
        "num_inputs": 100,
        "num_outputs": 1,
        "has_gradients": True,
    },
}

for dataset_name, dataset_info in datasets.items():
    # Extract data
    num_inputs = dataset_info["num_inputs"]
    num_outputs = dataset_info["num_outputs"]
    has_gradients = dataset_info["has_gradients"]

    # Load csv dataset
    df = read_csv(f"csv_datasets/{dataset_name}.csv", header=0, index_col=0)

    # Break it down
    input_names = df.columns[:num_inputs]
    output_names = df.columns[num_inputs : num_inputs + num_outputs]

    with File(f"datasets/{dataset_name}.h5", "w") as h5_file:
        # Inputs
        inputs_dataset = h5_file.create_dataset(
            "inputs", data=df[input_names].to_numpy()
        )
        inputs_dataset.attrs["input_names"] = [
            str(input_name) for input_name in input_names
        ]

        # Outputs
        outputs_group = h5_file.create_group("outputs")
        for output_name in output_names:
            outputs_group.create_dataset(
                output_name, data=df[output_name].to_numpy().reshape((-1, 1))
            )

        # Gradients (if applicable)
        if has_gradients:
            gradients_group = h5_file.create_group("gradients")
            for output_name in output_names:
                gradients_group.create_dataset(
                    output_name,
                    data=df[
                        [
                            f"d({output_name})_d({input_name})"
                            for input_name in input_names
                        ]
                    ].to_numpy(),
                )
