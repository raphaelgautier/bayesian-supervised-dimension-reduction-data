from pathlib import Path

import h5py
import numpy as np
import pandas as pd

su2_data_path = Path(
    "/".join(("/mnt", "d", "Cloud", "DropboxGT", "DR thesis datasets"))
)

CRM_INPUT_NAMES = [f"ffd_control_point_{i+1}" for i in range(50)]

RAE2822_INPUT_NAMES = ["angle_of_attack"] + CRM_INPUT_NAMES

RAE2822_OUTPUT_NAMES = [
    "LIFT",
    "DRAG",
    "EFFICIENCY",
    "FORCE_X",
    "FORCE_Y",
    "MOMENT_Z",
]

CRM_OUTPUT_NAMES = RAE2822_OUTPUT_NAMES + [
    "FORCE_Z",
    "MOMENT_X",
    "MOMENT_Y",
    "SIDEFORCE",
]


datasets = {
    "RAE2822_baseline_51DV_M0.725": {
        "path": "airfoil/RAE2822_M0.725_51DV_baseline.h5",
        "input_names": RAE2822_INPUT_NAMES,
        "output_names": RAE2822_OUTPUT_NAMES,
    },
    "CRM_450kgrid_twist_50DV_transonic": {
        "path": "crm/CRM-Wing-450k_twist_50DV_FFD-4-4-1.h5",
        "input_names": CRM_INPUT_NAMES,
        "output_names": CRM_OUTPUT_NAMES,
    },
    "CRM_450kgrid_twist_50DV_subsonic": {
        "path": "crm/CRM_Wing-450k_twist_50DV_FFD-4-4-1-sub.h5",
        "input_names": CRM_INPUT_NAMES,
        "output_names": CRM_OUTPUT_NAMES,
    },
}

for dataset_name, dataset_info in datasets.items():

    with h5py.File(su2_data_path / dataset_info["path"], "r") as h5_file:
        x = np.array(h5_file["STATE"]["VARIABLES"]["DV_VALUE_NEW"])
        y = np.stack(
            [
                np.array(h5_file["STATE"]["FUNCTIONS"][output_name])
                for output_name in dataset_info["output_names"]
            ],
            axis=1,
        )

    column_names = dataset_info["input_names"] + [
        output_name.lower() for output_name in dataset_info["output_names"]
    ]
    df = pd.DataFrame(data=np.concatenate((x, y), axis=1), columns=column_names)
    df.to_csv(f"{dataset_name}.csv")
